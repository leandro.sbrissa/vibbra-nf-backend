FROM node:current-alpine3.10

#ENV NODE_ENV=production

WORKDIR /usr/app

COPY . .

RUN yarn install --frozen-lockfile

RUN yarn build

CMD ["yarn", "start:prod"]
