# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### 0.0.6 (2021-10-27)


### Features

* added category feature ([b5bc7c8](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/b5bc7c8e16aaf07fcf005d574956b99a96200a10))
* added config feature ([c2d94b6](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/c2d94b61ae7026782d990c1a7b1ac1570be89a8c))
* added configs CRUD ([992cd1e](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/992cd1e3989349e243300f6b07000c793f723007))
* added CRUD category ([f92553f](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/f92553f1ca3e1cda8ebc20bdea97a340def271b4))
* added CRUD history NF ([eac53e0](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/eac53e08b590c9e2a53436d9f148f1fa56a00099))
* added delete expense ([3ba559a](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/3ba559a1d2f004bdaa2bfa59b1342f2e7b2a970b))
* added expense feature ([f6c7f39](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/f6c7f396e9d0da888acaa02952be93c563a6c3f4))
* added expenses by cliente feature ([45d43a7](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/45d43a7e12ad94d01d1c94c20ffc99d693d6e701))
* added expenses summary data ([243913c](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/243913c4a600c005ca587041957fcb0470f70df9))
* added feature check limits ([630bd8d](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/630bd8d070913d5c6c4410cf305e5662a4784b68))
* added histories feature ([c9f634f](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/c9f634f9fca5178f483a7c01ef3842237c5990fe))
* added SmsModule ([61d8c30](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/61d8c3052537eaad486e9b5e3abc5c2e8597d5d0))
* added summary histories feature ([4cf1abe](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/4cf1abe0536efb0d2ee238fe38379179232d8fac))
* added taks sending sms ([dbceb85](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/dbceb85419bff64233e036fe10016cbfc40cd0fa))
* added task sending SMS ([690b2e6](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/690b2e689f2eec7c25492bfef8424f6cf5607ecc))
* added test send config route ([9cfe6f4](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/9cfe6f435a87a4691f8c5d73c2308385faa3fdef))
* change database ([b74df0f](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/b74df0f72f0615c945b8d5f48ff0931e06277027))
* **client:** added clients feature ([fb1c82e](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/fb1c82e3ea881ae259c89b3c2186e14178176382))
* **deps:** added deps ([a146fdd](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/a146fdd9c87cbb0614b51acbb45ed019952cfb02))
* **deps:** added twilio deps ([f215edf](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/f215edf9af75e2390a2f5bd2820d412c284722d5))
* **expenses:** added CRUD expenses ([b7f824a](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/b7f824a25c5aeca4f0a5fda28afeb97a2ad46afc))
* function send sms ([0fa55ba](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/0fa55ba0332c611459d49b1af0a60623b90f8d6d))
* **helper:** added number utils ([e82fa34](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/e82fa34dc3fa856636db080c1db722aaa9e74e79))
* **history:** added history feature ([07b8403](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/07b8403402eb14c7003ee357589c8186269ed64a))
* prisma schema ([b72d9a0](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/b72d9a02f5ffe5415f31027c10bde0d7b77e0fd9))
* updated api documentation ([d97b2bd](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/d97b2bd75e9943cf6966136bbf13b072aeb82acd))


### Bug Fixes

* no-console ([c239aa4](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/c239aa41fa5d7623c1b871a88c5486b15db507ec))
* scope file name ([9d7913f](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/9d7913f2ef757dddd8bda03ea41417c0c81cb37b))
* scope file name ([c3ca39c](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/c3ca39c39a1111644b534db09fb3e2c94294a5d0))

### 0.0.5 (2021-10-27)


### Features

* added category feature ([b5bc7c8](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/b5bc7c8e16aaf07fcf005d574956b99a96200a10))
* added config feature ([c2d94b6](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/c2d94b61ae7026782d990c1a7b1ac1570be89a8c))
* added configs CRUD ([992cd1e](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/992cd1e3989349e243300f6b07000c793f723007))
* added CRUD category ([f92553f](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/f92553f1ca3e1cda8ebc20bdea97a340def271b4))
* added CRUD history NF ([eac53e0](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/eac53e08b590c9e2a53436d9f148f1fa56a00099))
* added expense feature ([f6c7f39](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/f6c7f396e9d0da888acaa02952be93c563a6c3f4))
* added expenses by cliente feature ([45d43a7](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/45d43a7e12ad94d01d1c94c20ffc99d693d6e701))
* added expenses summary data ([243913c](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/243913c4a600c005ca587041957fcb0470f70df9))
* added feature check limits ([630bd8d](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/630bd8d070913d5c6c4410cf305e5662a4784b68))
* added histories feature ([c9f634f](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/c9f634f9fca5178f483a7c01ef3842237c5990fe))
* added SmsModule ([61d8c30](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/61d8c3052537eaad486e9b5e3abc5c2e8597d5d0))
* added summary histories feature ([4cf1abe](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/4cf1abe0536efb0d2ee238fe38379179232d8fac))
* added taks sending sms ([dbceb85](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/dbceb85419bff64233e036fe10016cbfc40cd0fa))
* added task sending SMS ([690b2e6](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/690b2e689f2eec7c25492bfef8424f6cf5607ecc))
* added test send config route ([9cfe6f4](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/9cfe6f435a87a4691f8c5d73c2308385faa3fdef))
* change database ([b74df0f](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/b74df0f72f0615c945b8d5f48ff0931e06277027))
* **client:** added clients feature ([fb1c82e](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/fb1c82e3ea881ae259c89b3c2186e14178176382))
* **deps:** added deps ([a146fdd](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/a146fdd9c87cbb0614b51acbb45ed019952cfb02))
* **deps:** added twilio deps ([f215edf](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/f215edf9af75e2390a2f5bd2820d412c284722d5))
* **expenses:** added CRUD expenses ([b7f824a](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/b7f824a25c5aeca4f0a5fda28afeb97a2ad46afc))
* function send sms ([0fa55ba](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/0fa55ba0332c611459d49b1af0a60623b90f8d6d))
* **helper:** added number utils ([e82fa34](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/e82fa34dc3fa856636db080c1db722aaa9e74e79))
* **history:** added history feature ([07b8403](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/07b8403402eb14c7003ee357589c8186269ed64a))
* prisma schema ([b72d9a0](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/b72d9a02f5ffe5415f31027c10bde0d7b77e0fd9))
* updated api documentation ([d97b2bd](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/d97b2bd75e9943cf6966136bbf13b072aeb82acd))


### Bug Fixes

* scope file name ([9d7913f](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/9d7913f2ef757dddd8bda03ea41417c0c81cb37b))
* scope file name ([c3ca39c](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/c3ca39c39a1111644b534db09fb3e2c94294a5d0))

### 0.0.4 (2021-10-26)


### Features

* added category feature ([b5bc7c8](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/b5bc7c8e16aaf07fcf005d574956b99a96200a10))
* added config feature ([c2d94b6](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/c2d94b61ae7026782d990c1a7b1ac1570be89a8c))
* added configs CRUD ([992cd1e](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/992cd1e3989349e243300f6b07000c793f723007))
* added CRUD category ([f92553f](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/f92553f1ca3e1cda8ebc20bdea97a340def271b4))
* added CRUD history NF ([eac53e0](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/eac53e08b590c9e2a53436d9f148f1fa56a00099))
* added expense feature ([f6c7f39](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/f6c7f396e9d0da888acaa02952be93c563a6c3f4))
* added expenses summary data ([243913c](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/243913c4a600c005ca587041957fcb0470f70df9))
* added feature check limits ([630bd8d](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/630bd8d070913d5c6c4410cf305e5662a4784b68))
* added histories feature ([c9f634f](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/c9f634f9fca5178f483a7c01ef3842237c5990fe))
* added SmsModule ([61d8c30](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/61d8c3052537eaad486e9b5e3abc5c2e8597d5d0))
* added summary histories feature ([4cf1abe](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/4cf1abe0536efb0d2ee238fe38379179232d8fac))
* added taks sending sms ([dbceb85](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/dbceb85419bff64233e036fe10016cbfc40cd0fa))
* added task sending SMS ([690b2e6](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/690b2e689f2eec7c25492bfef8424f6cf5607ecc))
* added test send config route ([9cfe6f4](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/9cfe6f435a87a4691f8c5d73c2308385faa3fdef))
* change database ([b74df0f](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/b74df0f72f0615c945b8d5f48ff0931e06277027))
* **client:** added clients feature ([fb1c82e](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/fb1c82e3ea881ae259c89b3c2186e14178176382))
* **deps:** added deps ([a146fdd](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/a146fdd9c87cbb0614b51acbb45ed019952cfb02))
* **deps:** added twilio deps ([f215edf](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/f215edf9af75e2390a2f5bd2820d412c284722d5))
* **expenses:** added CRUD expenses ([b7f824a](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/b7f824a25c5aeca4f0a5fda28afeb97a2ad46afc))
* function send sms ([0fa55ba](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/0fa55ba0332c611459d49b1af0a60623b90f8d6d))
* **helper:** added number utils ([e82fa34](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/e82fa34dc3fa856636db080c1db722aaa9e74e79))
* **history:** added history feature ([07b8403](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/07b8403402eb14c7003ee357589c8186269ed64a))
* prisma schema ([b72d9a0](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/b72d9a02f5ffe5415f31027c10bde0d7b77e0fd9))


### Bug Fixes

* scope file name ([9d7913f](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/9d7913f2ef757dddd8bda03ea41417c0c81cb37b))
* scope file name ([c3ca39c](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/c3ca39c39a1111644b534db09fb3e2c94294a5d0))

### 0.0.3 (2021-10-26)


### Features

* added category feature ([b5bc7c8](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/b5bc7c8e16aaf07fcf005d574956b99a96200a10))
* added config feature ([c2d94b6](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/c2d94b61ae7026782d990c1a7b1ac1570be89a8c))
* added configs CRUD ([992cd1e](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/992cd1e3989349e243300f6b07000c793f723007))
* added CRUD category ([f92553f](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/f92553f1ca3e1cda8ebc20bdea97a340def271b4))
* added CRUD history NF ([eac53e0](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/eac53e08b590c9e2a53436d9f148f1fa56a00099))
* added expense feature ([f6c7f39](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/f6c7f396e9d0da888acaa02952be93c563a6c3f4))
* added expenses summary data ([243913c](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/243913c4a600c005ca587041957fcb0470f70df9))
* added histories feature ([c9f634f](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/c9f634f9fca5178f483a7c01ef3842237c5990fe))
* added summary histories feature ([4cf1abe](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/4cf1abe0536efb0d2ee238fe38379179232d8fac))
* change database ([b74df0f](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/b74df0f72f0615c945b8d5f48ff0931e06277027))
* **client:** added clients feature ([fb1c82e](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/fb1c82e3ea881ae259c89b3c2186e14178176382))
* **deps:** added deps ([a146fdd](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/a146fdd9c87cbb0614b51acbb45ed019952cfb02))
* **expenses:** added CRUD expenses ([b7f824a](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/b7f824a25c5aeca4f0a5fda28afeb97a2ad46afc))
* **helper:** added number utils ([e82fa34](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/e82fa34dc3fa856636db080c1db722aaa9e74e79))
* **history:** added history feature ([07b8403](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/07b8403402eb14c7003ee357589c8186269ed64a))
* prisma schema ([b72d9a0](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/b72d9a02f5ffe5415f31027c10bde0d7b77e0fd9))


### Bug Fixes

* scope file name ([9d7913f](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/9d7913f2ef757dddd8bda03ea41417c0c81cb37b))
* scope file name ([c3ca39c](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/c3ca39c39a1111644b534db09fb3e2c94294a5d0))

### 0.0.2 (2021-10-22)


### Features

* added category feature ([b5bc7c8](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/b5bc7c8e16aaf07fcf005d574956b99a96200a10))
* added config feature ([c2d94b6](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/c2d94b61ae7026782d990c1a7b1ac1570be89a8c))
* added expense feature ([f6c7f39](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/f6c7f396e9d0da888acaa02952be93c563a6c3f4))
* change database ([b74df0f](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/b74df0f72f0615c945b8d5f48ff0931e06277027))
* **client:** added clients feature ([fb1c82e](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/fb1c82e3ea881ae259c89b3c2186e14178176382))
* **history:** added history feature ([07b8403](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/07b8403402eb14c7003ee357589c8186269ed64a))
* prisma schema ([b72d9a0](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-backend/commit/b72d9a02f5ffe5415f31027c10bde0d7b77e0fd9))
