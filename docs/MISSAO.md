# Missão

## 1 Avaliação do escopo
Escopo está claro, sem dúvidas pendentes.
Veja o detalhamento do escopo aqui: 

[escopo do backend](./SCOPE.MD) 

[escopo do frontend](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-frontend/-/blob/master/docs/SCOPE.MD)


--- 

## 2 Estimativa em horas

### Backend 
> ** Tempo detalhado no apontamento

| Atividade | Previsto | Trabalhado |
|:----------|:--------:|:----------:|
| Configuração de ambiente DEV |  1h  | 30m |
| Recursos básicos da API |  1h   | 1h 30m |
| Regras de negócio  |  10h  | 8h 30m |
| Regras de negócio  |  10h  | 7h 30m |
| Semear banco de dados |  1h  | 30m |
| Testes de Integração |  8h  | -- |
| Documentação da API |  8h  | -- |
| Deploy | 1h | 1h |
| Configuração CI/CD |  4h  | -- |
| | | |
| total | 34h | 12h 30m |

### Frontend 
> ** Tempo detalhado no apontamento

| Funcionalidade | Previsto | Trabalhado |
|:----------|:--------:|:----------:|
| configuração de ambiente DEV |  1h  | ~1h 30m |
|  |  |  |
| *Configuração de autenticação* | 8h | -- |
| *Configuração de Api* | 3h | -- |
| *Tratamento de erros API* | 2h | -- |
| *Componentização* | 12h | -- |
| *Armazenamento local (store/storage)* | 3h | -- |
| Menu principal + login/logout |  1h | ~1h 30m |
| Configuração do sistema | 1h | ~2h |
| Cadastro de empresas |  1h  | ~2h |
| Cadastro de categorias |  1h  | ~2h |
| Cadastro de despesas |  1h  | ~3h 30m |
| Cadastro de notas fiscais |  2h  | ~3h |
| Gráficos da tela principal |  2h  | ~4h |
| *Teste de UI/UX* | ~4h | -- |
| Implementação de Testes de Integração |  12h  | -- |
| Configuração CI/CD |  4h  | -- |
|  | | |
| total | 58h | ~21h 30m |

### Relatório final de entrega

| Atividade | Previsto | Trabalhado |
|:----------|:--------:|:----------:|
| preparação de entrega | 1h | -- |

| Atividade | Previsto | Trabalhado |
|:----------|:--------:|:----------:|
| Backend | 34h | ~13h |
| Frontend | 58h | ~21h 30m |
| |  |  |
| Total | ~92h | ~35h |

---

## 3 Estimativa em dias
Para o projeto completo é estimado um tempo total de **15 dias úteis**. 
> É importante salientar que muitas implementações e atividades, foram suprimidas pelo fato do projeto ser caracterizado como **teste simples** sendo necessário apenas **7 dias** para uma amostra de funcionamento.


## 4 Apontamento de horas para o teste
O apontamento de horas será inserido na própria plataforma **Vibbra**.

## 5 Tecnologias e funcionalidades  do Teste

**Backend:** Para o backend escolhi utilizar a framework [NestJs](http://nestjs.com/) e [Prisma.io](https://www.prisma.io/) que possibilita o implantação rápida de uma *REST API*. Banco de dados relacional [MySQL](https://www.mysql.com/) porque já possuo uma hospedagem e estrutura pronta e funcional.

**Frontend:** Para o front escolhi utilizar minha framework preferida [React Js](https://pt-br.reactjs.org/), hospedado na [Vercel](https://vercel.com/). Componentização com [Material UI](https://mui.com/) para otimizar o tempo na criação de componentes. Também utilizei um *boilerplate* customizado para agilizar ao máximo o inicio das implementações.