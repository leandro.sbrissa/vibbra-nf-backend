<p>
<a href="" target="blank"><img src="./docs/vibbra/logo.svg" height="44"></a>
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="120" alt="Nest Logo" /></a> <a href="https://www.prisma.io/" target="blank"><img src="https://raw.githubusercontent.com/prisma/presskit/master/Logos/Logo%20Black.svg" width="120"></a> 
</p>

# vibbra-nf-backend

Access the links below to view project delivery documentation:

[Mission Information PT-BR](./docs/MISSAO.md)

[PROJECT SCOPE PT-BR](./docs/SCOPE.MD)

## Description

This is an API application to be consumed by the [vibbra-nf-frontend](http://git.vibbra.com.br/leandro-1631421558/vibbra-nf-frontend). Built for testing purposes for `Vibbra`. If everything is correct and the delivery has been made, you can view the documentation in production here: 
[API Docs](https://vibbra-nf-backend.avatarsolucoesdigitais.com.br/docs/) 
[Frontend production](https://vibbra-nf-frontend.vercel.app)

*API - [Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.*

## Installation
 For development, the application uses **MySql 8** database.

**1) Environment**
Edit the `.env` file according to your development environment.

```.env
NODE_ENV=development
DATABASE_URL="mysql://vibbra-nf:vibbra@2021@localhost:3306/vibbra-nf"
SALT_KEY=SECRET
```

**2) Install dependencies**

```bash
$ yarn install
```

## Development

The `docker-compose.yml` file at the root of the project is used to deploy to **production**, for a local database use `docker-compose-local.yml`. See: [Docker](https://docs.docker.com/)

**1) Set up Prisma (Typescrip ORM)**

The project uses [Prisma.io](https://www.prisma.io/) as ORM, for more information see: https://www.prisma.io/docs/getting-started/

```bash
$ yarn prisma generate dev
```

**2) Application in development**

Start the application in development mode

```bash
# development
$ yarn start

# watch mode
$ yarn start:dev
```

**3) Seeding database**
The parameters for database seed are in `./prisma/seed.ts` and `./docs/seeds`


```bash
# development
$ yarn prisma db seed
```

## Running production

**1) Build production**

```bash
# generate bundle
$ yarn build

# production mode
$ yarn start:prod
```

**2) Proxy configuration**

The production server uses **nginx** proxy and automated **SSL certificate (Let's Encrypt)** generator. The following ​​environment variables are for the production only:

```
VIRTUAL_HOST=vibbra-nf-backend.avatarsolucoesdigitais.com.br
LETSENCRYPT_HOST=vibbra-nf-backend.avatarsolucoesdigitais.com.br
LETSENCRYPT_EMAIL=<address@email.com>
```

See details here:
- nginx-proxy [@jwilder](https://github.com/jwilder/nginx-proxy)
- docker-gen [@jwilder](https://github.com/jwilder/docker-gen)
- docker-letsencrypt-nginx-proxy-companion [@JrCs](https://github.com/JrCs/docker-letsencrypt-nginx-proxy-companion)

https://github.com/evertramos/docker-compose-letsencrypt-nginx-proxy-companion

**3) Notification service configuration**

SMS notification service implemented with [twilio](https://www.twilio.com/). Generate credentials from the control panel.

```
SMTP_HOST=mail.example.com
SMTP_USER=email@example.com
SMTP_PASS=password
SMTP_PORT=587

TWILIO_ACCOUNT_SID=SID
TWILIO_AUTH_TOKEN=TOKEN
TWILIO_PHONE_NUMBER=NUMBER
```

---


### [CHANGELOG](./docs/CHANGELOG.md)

## License

[MIT licensed](LICENSE).
