import { lorem } from 'faker/locale/pt_BR';

import { Category } from '@prisma/client';

export const categories: Category[] = [
  { id: 1, name: `Alimentação`, description: lorem.sentence(), actived: true },
  { id: 2, name: `Manutenção`, description: lorem.sentence(), actived: true },
  { id: 3, name: `Aluguéis`, description: lorem.sentence(), actived: true },
  { id: 4, name: `Segurança`, description: lorem.sentence(), actived: true },
  { id: 5, name: `Comissões`, description: lorem.sentence(), actived: true },
  { id: 6, name: `Insumos`, description: lorem.sentence(), actived: true },
  { id: 7, name: `Limpeza`, description: lorem.sentence(), actived: true },
  { id: 8, name: `Horas extras`, description: lorem.sentence(), actived: true },
  { id: 9, name: `Transporte`, description: lorem.sentence(), actived: true },
];
