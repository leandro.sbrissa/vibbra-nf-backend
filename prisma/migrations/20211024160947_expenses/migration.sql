/*
  Warnings:

  - You are about to drop the column `actived` on the `expenses` table. All the data in the column will be lost.
  - You are about to drop the column `description` on the `expenses` table. All the data in the column will be lost.
  - Added the required column `date` to the `expenses` table without a default value. This is not possible if the table is not empty.

*/
-- DropIndex
DROP INDEX `actived` ON `expenses`;

-- AlterTable
ALTER TABLE `expenses` DROP COLUMN `actived`,
    DROP COLUMN `description`,
    ADD COLUMN `clientId` INTEGER,
    ADD COLUMN `date` DATE NOT NULL,
    ADD COLUMN `payDate` DATE,
    ADD COLUMN `value` DECIMAL(10, 2) NOT NULL DEFAULT 0.00;

-- AddForeignKey
ALTER TABLE `expenses` ADD CONSTRAINT `expenses_clientId_fkey` FOREIGN KEY (`clientId`) REFERENCES `clients`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;
