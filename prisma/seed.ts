import { cnpj } from 'cpf-cnpj-validator';
import { parse, add, format } from 'date-fns';
import { name, company, commerce, date, lorem } from 'faker/locale/pt_BR';
import { existsSync, readFileSync, writeFileSync } from 'fs';
import { resolve, join } from 'path';

import { PrismaClient, Client, Expense, History } from '@prisma/client';
import { Decimal } from '@prisma/client/runtime';

import { categories } from './seeds/categories';
import { users } from './seeds/users';

function genDate(str: string) {
  return parse(str, 'yyyy-MM-dd', new Date());
}

function compareValues<T = unknown>(key: keyof T, order = 'asc') {
  return function innerSort(a: any, b: any) {
    if (!(key in a) || !(key in b)) return 0;
    const varA = typeof a[key] === 'string' ? a[key].toUpperCase() : a[key];
    const varB = typeof b[key] === 'string' ? b[key].toUpperCase() : b[key];

    let comparison = 0;
    if (varA > varB) {
      comparison = 1;
    } else if (varA < varB) {
      comparison = -1;
    }
    return order === 'desc' ? comparison * -1 : comparison;
  };
}

function randomInt(min: number, max: number): number {
  const m = Math.ceil(min);
  return Math.floor(Math.random() * (Math.floor(max) - m + 1)) + m;
}

const prisma = new PrismaClient();
const savePath = resolve(__dirname, '../docs', 'seeds');

function saveToJson(fileName: string, list: any[]) {
  writeFileSync(fileName, JSON.stringify(list), { encoding: 'utf-8' });
}

const generateClients = (max = 10): Partial<Client>[] => {
  const generate = () => {
    const result: Partial<Client>[] = [];
    for (let i = 0; i < max; i++) {
      result.push({
        id: i + 1,
        name: name.firstName(),
        cnpj: cnpj.generate(),
        socialName: company.companyName(),
        actived: true,
      });
    }
    return result;
  };

  const file = join(savePath, 'clients.json');
  const hasFile = existsSync(file);
  if (hasFile) {
    const rawdata = readFileSync(file, { encoding: 'utf-8' });
    return JSON.parse(rawdata);
  }
  const list = generate();
  saveToJson(file, list || []);
  return list;
};

const generateExpenses = (max = 10, clientIds: number[] = [], categoryIds: number[] = []): Expense[] => {
  const generate = () => {
    const result: Partial<Expense>[] = [];

    for (let i = 0; i < max; i++) {
      const d = date.between(genDate('2015-01-01'), new Date());
      const pD = date.between(d, add(d, { days: 7 }));
      result.push({
        id: i + 1,
        name: `${commerce.product()} ${commerce.productAdjective()}`,
        value: new Decimal(commerce.price(10, 299, 2)),
        categoryId: categoryIds[Math.floor(Math.random() * categoryIds.length)],
        clientId: clientIds[Math.floor(Math.random() * clientIds.length)],
        date: d,
        payDate: pD,
      });
    }
    return result;
  };
  const file = join(savePath, 'expenses.json');
  const hasFile = existsSync(file);
  if (hasFile) {
    const rawdata = readFileSync(file, { encoding: 'utf-8' });
    return JSON.parse(rawdata);
  }
  const list = generate();
  saveToJson(file, list || []);
  return list as Expense[];
};

const generateHistories = (max = 10, clientIds: number[] = []): History[] => {
  const generate = () => {
    const result: Partial<History>[] = [];

    for (let i = 0; i < max; i++) {
      const d = date.between(genDate('2015-01-01'), new Date());
      result.push({
        nfNumber: `${randomInt(100000, 999999)}`,
        value: new Decimal(commerce.price(10, 999, 2)),
        clientId: clientIds[Math.floor(Math.random() * clientIds.length)],
        date: d,
        month: parseInt(format(d, 'M'), 10),
        description: lorem.sentence(),
      });
    }
    return result;
  };
  const file = join(savePath, 'histories.json');
  const hasFile = existsSync(file);
  if (hasFile) {
    const rawdata = readFileSync(file, { encoding: 'utf-8' });
    return JSON.parse(rawdata);
  }
  const list = generate().sort(compareValues('date', 'asc'));
  saveToJson(file, list || []);
  return list as History[];
};

async function main() {
  await Promise.all(
    users.map(async user => {
      await prisma.user.upsert({ where: { email: user.email }, update: user, create: user });
    }),
  );

  const clients = generateClients(50);

  await Promise.all(
    clients.map(async client => {
      const where = client?.id ? { id: client.id } : { cnpj: client.cnpj };
      await prisma.client.upsert({ where, update: client, create: client });
    }),
  );

  await Promise.all(
    categories.map(async category => {
      await prisma.category.upsert({
        update: category,
        create: category,
        where: { id: category.id },
      });
    }),
  );

  const expenses = generateExpenses(
    500,
    clients.map(c => c.id),
    categories.map(c => c.id),
  );

  await Promise.all(
    expenses.map(async expense => {
      await prisma.expense.upsert({ update: expense, create: expense, where: { id: expense.id } });
    }),
  );

  const histories = generateHistories(
    1000,
    clients.map(c => c.id),
  );

  await Promise.all(
    histories.map(async history => {
      await prisma.history.upsert({ update: history, create: history, where: { nfNumber: history.nfNumber } });
    }),
  );
}

main()
  .catch(e => {
    console.error(e);
    process.exit(1);
  })
  .finally(async () => {
    console.log('END');
    await prisma.$disconnect();
  });
