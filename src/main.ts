import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import type { NestExpressApplication } from '@nestjs/platform-express';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

import { css } from './config/swagger.css';
import { AppModule } from './useCases/app/app.module';

async function bootstrap() {
  const app: NestExpressApplication = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe());

  const options = new DocumentBuilder()
    .setTitle('Test Vibbra NF')
    .setDescription('Test application for vibrating platform')
    .setVersion('v1')
    .addBearerAuth({ type: 'http', scheme: 'bearer', bearerFormat: 'JWT', in: 'header' }, 'JWT')
    .build();
  const document = SwaggerModule.createDocument(app, options);

  SwaggerModule.setup('docs', app, document, {
    swaggerOptions: { persistAuthorization: true },
    customCss: css,
  });

  app.enableCors();

  const httpPort = Number(process.env.HTTP_PORT || 3333);
  await app.listen(httpPort);
  // eslint-disable-next-line no-console
  console.log('api runing', httpPort);
}
bootstrap();
