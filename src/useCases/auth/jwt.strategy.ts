import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';

import { AuthService } from './auth.service';
import { CurrentAuthDto, PayloadTokenDto } from './dto/payload-token.dto';

// type Writeable<T> = { -readonly [P in keyof T]-?: T[P] };

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(public authService: AuthService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: process.env.SALT_KEY,
    });
  }

  async validate(payload: PayloadTokenDto): Promise<CurrentAuthDto> {
    const result: CurrentAuthDto = { ...payload };
    const { exp, uuid } = payload;
    if (!exp && uuid) {
      const token = await this.authService.validateUUID(uuid);
      if (!token) {
        // resolve erro token revogado
        throw new UnauthorizedException('revoked_token');
      }
      if (!token?.user?.actived) {
        // resolve erro de usuario inativo
        throw new UnauthorizedException('disabled_user');
      }
      result.email = token?.user?.email || payload?.email;
      // result.level = token?.user?.level || payload?.level;
    }

    return result;
  }
}
