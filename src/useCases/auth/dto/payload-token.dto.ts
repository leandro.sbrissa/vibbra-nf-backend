import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty } from 'class-validator';

import { ResponseApiDto } from '../../app/dto/response-api.dto';

export class SignInDto {
  @ApiProperty()
  @IsEmail()
  readonly email: string;

  @ApiProperty()
  @IsNotEmpty()
  readonly password: string;
}

export class PayloadTokenDto {
  readonly id: number;
  readonly uuid?: string;
  readonly email: string;
  readonly level: number;
  readonly iat?: number;
  readonly exp?: number;
}

export class CurrentAuthDto {
  readonly id: number;
  email: string;
  level: number;
  iat?: number;
  exp?: number;
}

export class RefreshTokenDto {
  @ApiProperty()
  @IsNotEmpty()
  readonly refreshToken: string;
}

export class ResponseSignedDto extends ResponseApiDto {
  token: string;
  refreshToken?: string;
  user?: PayloadTokenDto;
}
