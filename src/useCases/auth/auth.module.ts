import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { config } from 'dotenv';

import { TokensModule } from '../tokens/tokens.module';
import { UsersModule } from '../users/users.module';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { GoogleStrategy } from './google.strategy';
import { JwtStrategy } from './jwt.strategy';
import { LocalStrategy } from './local.strategy';

config();

@Module({
  imports: [
    UsersModule,
    TokensModule,
    PassportModule,
    // registerJWT,
    JwtModule.registerAsync({
      // imports: [ConfigModule],
      useFactory: async () => {
        return {
          secret: process.env.SALT_KEY,
          // signOptions: { expiresIn: '1d' },
        };
      },
    }),
  ],
  providers: [AuthService, GoogleStrategy, LocalStrategy, JwtStrategy],

  controllers: [AuthController],
  exports: [AuthService],
})
export class AuthModule {}
