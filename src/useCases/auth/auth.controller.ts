import { Controller, Get, Post, Req, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBody, ApiResponse } from '@nestjs/swagger';

import { CurrentAuth } from './auth.decorator';
import { AuthService } from './auth.service';
import { PayloadTokenDto, ResponseSignedDto, SignInDto } from './dto/payload-token.dto';
import { LocalAuthGuard } from './local-auth.guard';
import { Public } from './public.decorator';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Get('/google')
  @UseGuards(AuthGuard('google'))
  async googleAuth(@Req() _req: any) {
    // console.log('req', req);
  }

  @Get('google/redirect')
  @UseGuards(AuthGuard('google'))
  googleAuthRedirect(@Req() req: any) {
    return this.authService.googleLogin(req);
  }

  @Public()
  @ApiBody({ type: SignInDto })
  @ApiResponse({ type: ResponseSignedDto })
  @UseGuards(LocalAuthGuard)
  @Post()
  async login(@CurrentAuth() auth: PayloadTokenDto): Promise<ResponseSignedDto> {
    const login = await this.authService.login(auth);

    return {
      success: true,
      user: {
        id: auth.id,
        email: auth.email,
        level: auth.level,
      },
      token: login.accessToken,
      // refreshToken: login.refreshToken,
    };
  }
}
