import { Injectable, UnauthorizedException, BadRequestException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-local';

import { AuthService } from './auth.service';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private authService: AuthService) {
    super({ usernameField: 'email' });
  }

  async validate(username: string, password: string) {
    if (!username || !password) {
      throw new BadRequestException('invalid_credentials');
    }

    const user = await this.authService.validateUser(username, password);
    if (!user) {
      throw new UnauthorizedException('invalid_credentials');
    }

    if (!user.actived) {
      throw new UnauthorizedException('disabled_user');
    }

    return user;
  }
}
