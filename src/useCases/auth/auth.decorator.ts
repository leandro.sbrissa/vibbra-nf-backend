import { createParamDecorator, ExecutionContext } from '@nestjs/common';

import { CurrentAuthDto } from './dto/payload-token.dto';

export type { CurrentAuthDto };

export const CurrentAuth = createParamDecorator((data: keyof CurrentAuthDto, ctx: ExecutionContext) => {
  const request = ctx.switchToHttp().getRequest();
  const auth = request.user as CurrentAuthDto;
  return data ? auth[data] : auth;
});
