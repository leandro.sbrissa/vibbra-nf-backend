import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

import { TokensService } from '../tokens/tokens.service';
import { UsersService } from '../users/users.service';
import { PayloadTokenDto } from './dto/payload-token.dto';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private tokensService: TokensService,
    private jwtService: JwtService,
  ) {}

  googleLogin(req: any) {
    if (!req.user) {
      return 'No user from google';
    }

    return {
      message: 'User information from google',
      user: req.user,
    };
  }

  async validateUser(email: string, pass: string) {
    const user = await this.usersService.autenticate(email, pass);
    if (user) {
      return user;
    }
    return null;
  }

  async validateUUID(uid: string) {
    const token = await this.tokensService.findByUuid(uid);
    return token;
  }

  async login(user: PayloadTokenDto) {
    const payload = { id: user.id, email: user.email, level: user.level };
    const accessToken = this.jwtService.sign(payload, { expiresIn: '365d' });
    // const refreshToken = await this.createRefreshToken(user.id);

    return {
      accessToken,
      // refreshToken,
    };
  }
}
