import { BadRequestException, Injectable } from '@nestjs/common';
import { createHash } from 'crypto';

import { User } from '@prisma/client';

import { PrismaService } from '../../prisma/prisma.service';
import { CreateUserDto } from './dto/create-user.dto';

@Injectable()
export class UsersService {
  constructor(private prisma: PrismaService) {}

  async autenticate(email: string, password: string): Promise<User> {
    const hash = createHash('md5').update(`${password}`).digest('hex');
    const user = await this.prisma.user.findFirst({ where: { email, password: hash } });
    return user;
  }

  async findOne(id: number) {
    const user = await this.prisma.user.findUnique({ where: { id } });
    delete user.password;
    return user;
  }

  async create({ password, ...rest }: CreateUserDto): Promise<number> {
    try {
      const pwd = createHash('md5').update(`${password}`).digest('hex');
      const user = await this.prisma.user.create({ data: { ...rest, password: pwd } });
      return user.id;
    } catch (error) {
      throw new BadRequestException(error?.message);
    }
  }
}
