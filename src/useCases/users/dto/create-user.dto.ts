import { Exclude } from 'class-transformer';
import { IsBoolean, IsEmail, IsInt, IsOptional, IsString } from 'class-validator';

export class CreateUserDto {
  @IsInt()
  @IsOptional()
  id?: number;

  @IsString()
  name: string;

  @IsEmail()
  email: string;

  @IsInt()
  level: number;

  @IsBoolean()
  @IsOptional()
  actived?: boolean;

  @Exclude()
  password: string;
}
