import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';

import { CurrentAuth } from '../auth/auth.decorator';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { Public } from '../auth/public.decorator';
import { CreateUserDto } from './dto/create-user.dto';
import { UsersService } from './users.service';

// @ApiExcludeController(true)
@ApiTags('users')
@UseGuards(JwtAuthGuard)
@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @ApiBearerAuth('JWT')
  @Get('me')
  async findMe(@CurrentAuth('id') userId: number) {
    const user = await this.usersService.findOne(userId);
    return {
      success: true,
      user,
    };
  }

  @Public()
  @Post('me')
  async createMe(@Body() userData: CreateUserDto) {
    const userId = await this.usersService.create(userData);
    return {
      success: !!userId,
      userId,
    };
  }
}
