import { IsBoolean, IsOptional, IsString } from 'class-validator';

import { ResponseApiDto } from '../../app/dto/response-api.dto';

export class CategoryDto {
  id: number;
  name: string;
  description: string;
  actived?: boolean;
}

export class CreateCategoryDto {
  @IsString()
  name: string;

  @IsString()
  description: string;

  @IsBoolean()
  @IsOptional()
  actived?: boolean;
}

export class ResponseCategoriesDto extends ResponseApiDto {
  categories: CategoryDto[];
}

export class ResponseCategoryDto extends ResponseApiDto {
  categoryId?: number;
  category?: CategoryDto;
}
