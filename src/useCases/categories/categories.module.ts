import { Module } from '@nestjs/common';

import { PrismaService } from '../../prisma/prisma.service';
import { CategoriesController } from './categories.controller';
import { CategoriesService } from './categories.service';

@Module({
  controllers: [CategoriesController],
  providers: [PrismaService, CategoriesService],
  exports: [CategoriesService],
})
export class CategoriesModule {}
