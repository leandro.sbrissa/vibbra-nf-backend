import { Controller, Get, Post, Body, Patch, Param, Delete, ParseIntPipe } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';

import { CategoriesService } from './categories.service';
import { CreateCategoryDto, ResponseCategoriesDto, ResponseCategoryDto } from './dto/create-category.dto';
import { UpdateCategoryDto } from './dto/update-category.dto';

@ApiBearerAuth('JWT')
@ApiTags('categories')
@Controller('categories')
export class CategoriesController {
  constructor(private readonly categoriesService: CategoriesService) {}

  @Post()
  async create(@Body() createCategoryDto: CreateCategoryDto): Promise<ResponseCategoryDto> {
    const categoryId = await this.categoriesService.create(createCategoryDto);
    return {
      success: true,
      categoryId,
    };
  }

  @Get()
  async findAll(): Promise<ResponseCategoriesDto> {
    const categories = await this.categoriesService.findAll();
    return {
      success: true,
      categories,
    };
  }

  @Get(':id')
  async findOne(@Param('id', ParseIntPipe) id: number): Promise<ResponseCategoryDto> {
    const category = await this.categoriesService.findOne(id);
    return {
      success: true,
      category,
    };
  }

  @Patch(':id')
  async update(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateCategoryDto: UpdateCategoryDto,
  ): Promise<ResponseCategoryDto> {
    const category = await this.categoriesService.update(id, updateCategoryDto);
    return {
      success: true,
      categoryId: category.id,
      category,
    };
  }

  @Delete(':id')
  remove(@Param('id', ParseIntPipe) id: number) {
    return this.categoriesService.remove(id);
  }
}
