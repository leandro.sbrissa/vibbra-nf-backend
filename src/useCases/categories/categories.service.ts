import { Injectable } from '@nestjs/common';

import { Prisma } from '.prisma/client';

import { PrismaService } from '../../prisma/prisma.service';
import { CategoryDto, CreateCategoryDto } from './dto/create-category.dto';
import { UpdateCategoryDto } from './dto/update-category.dto';

@Injectable()
export class CategoriesService {
  constructor(private prisma: PrismaService) {}

  async create(createCategoryDto: CreateCategoryDto) {
    const result = await this.prisma.category.create({ data: createCategoryDto });
    return result.id;
  }

  async findAll(query?: Prisma.CategoryFindManyArgs) {
    const result = await this.prisma.category.findMany({ orderBy: { name: 'asc' }, ...query });
    return result as CategoryDto[];
  }

  async findOne(id: number) {
    const result = await this.prisma.category.findUnique({ where: { id } });
    return result;
  }

  async update(id: number, updateCategoryDto: UpdateCategoryDto) {
    const result = await this.prisma.category.update({ where: { id }, data: updateCategoryDto });
    return result;
  }

  async remove(id: number) {
    try {
      await this.prisma.category.delete({ where: { id } });
      return true;
    } catch (error) {
      return false;
    }
  }
}
