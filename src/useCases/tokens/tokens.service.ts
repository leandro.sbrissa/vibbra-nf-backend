import { Injectable } from '@nestjs/common';

import { Prisma } from '@prisma/client';

import { PrismaService } from '../../prisma/prisma.service';
@Injectable()
export class TokensService {
  constructor(private prisma: PrismaService) {}

  async create(data: Prisma.TokenCreateInput) {
    const token = await this.prisma.token.create({ data });
    return token;
  }

  async findByUuid(uuid: string) {
    const token = await this.prisma.token.findFirst({ where: { uuid, actived: true }, include: { user: true } });
    return token;
  }

  async findOne(idOrToken: number | string) {
    const filter: Prisma.TokenWhereInput =
      typeof idOrToken === 'number' ? { id: idOrToken } : { refreshToken: idOrToken, actived: true };
    const token = await this.prisma.token.findFirst({ where: filter, include: { user: true } });
    return token;
  }

  async update(id: number, data: Prisma.TokenUpdateInput) {
    const token = await this.prisma.token.update({ where: { id }, data });
    return token;
  }

  async remove(id: number) {
    const token = await this.prisma.token.delete({ where: { id } });
    return token;
  }
}
