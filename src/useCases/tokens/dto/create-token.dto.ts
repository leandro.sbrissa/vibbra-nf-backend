export class CreateTokenDto {
  id?: number;
  uuid?: string;
  userId: number;
  token?: string;
  refreshToken?: string;
  expires?: Date | string;
  actived: boolean;
}
