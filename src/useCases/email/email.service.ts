import { MailerService } from '@nestjs-modules/mailer';
import { Injectable, Logger, OnModuleInit } from '@nestjs/common';

import { SendMailDto } from './dto/email.dto';

@Injectable()
export class EmailService implements OnModuleInit {
  private readonly logger = new Logger(EmailService.name);

  constructor(private readonly mailerService: MailerService) {}

  async onModuleInit() {
    this.logger.log(`init`);
  }

  async sendEmail(message: SendMailDto): Promise<boolean> {
    this.logger.log(`SENDING TO: ${message.to}`);
    try {
      await this.mailerService.sendMail({
        to: message.to,
        subject: `${message.subject}`,
        text: message.message,
      });
      return true;
    } catch (error) {
      this.logger.error(`SENDING TO: ${message.to}`);
      return false;
    }
  }
}
