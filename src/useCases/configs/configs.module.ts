import { Module } from '@nestjs/common';

import { PrismaService } from '../../prisma/prisma.service';
import { EmailModule } from '../email/email.module';
import { SmsModule } from '../sms/sms.module';
import { ConfigsController } from './configs.controller';
import { ConfigsService } from './configs.service';

@Module({
  imports: [SmsModule, EmailModule],
  controllers: [ConfigsController],
  providers: [PrismaService, ConfigsService],
  exports: [ConfigsService],
})
export class ConfigsModule {}
