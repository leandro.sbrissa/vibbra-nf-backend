import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsOptional, IsPhoneNumber, IsString } from 'class-validator';

export class RequestStoreConfigDto {
  @ApiProperty({ description: 'chave da configuração' })
  @IsString()
  key: string;
}

export class RequestTesteConfigDto {
  @ApiProperty({ description: 'endereço de e-mail para teste' })
  @IsEmail()
  @IsOptional()
  email?: string;

  @ApiProperty({ description: 'numero de telefone para teste', example: '+5511999999999', title: 'formato BR' })
  @IsPhoneNumber('BR')
  @IsOptional()
  sms?: string;
}
