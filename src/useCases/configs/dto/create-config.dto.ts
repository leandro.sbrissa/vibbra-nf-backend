export class ConfigRevenueDto {
  maximum: number;
  quota: number;
  danger: number;
}

export class ConfigNotifyDto {
  email: {
    enabled: boolean;
    to: string;
  };
  sms: {
    enabled: boolean;
    to: string;
  };
}

export class CreateConfigDto {
  key: string;
  value: ConfigRevenueDto | ConfigNotifyDto | any;
}

export class ConfigContextDto {
  revenue: ConfigRevenueDto;
  notify: ConfigNotifyDto;
}
// export class RequestStoreConfigDto {

//   value: ConfigRevenueDto | ConfigNotifyDto | any;
// }
