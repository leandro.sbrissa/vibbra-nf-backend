import { Injectable } from '@nestjs/common';

import { Prisma } from '.prisma/client';

import { PrismaService } from '../../prisma/prisma.service';
import { ConfigContextDto, CreateConfigDto } from './dto/create-config.dto';
import { UpdateConfigDto } from './dto/update-config.dto';

@Injectable()
export class ConfigsService {
  constructor(private prisma: PrismaService) {}

  async store(key: string, value: any) {
    const result = await this.prisma.config.upsert({ where: { key }, update: { value }, create: { key, value } });
    return result.id;
  }

  async create(createConfigDto: CreateConfigDto) {
    const result = await this.prisma.config.create({ data: createConfigDto });
    return result.id;
  }

  async findAll(query?: Prisma.ConfigFindManyArgs) {
    const result = await this.prisma.config.findMany(query);
    return result;
  }

  async findOne(key: string) {
    const result = await this.prisma.config.findUnique({ where: { key } });
    return result;
  }

  async findByKey(keys: (keyof ConfigContextDto)[]) {
    const or = keys.map(key => ({ key }));
    const where = { OR: or } as Prisma.ConfigFindUniqueArgs['where'];

    const data = await this.prisma.config.findMany({ where });
    const result = data.reduce((acc, item) => {
      acc[item.key] = item.value;
      return acc;
    }, {}) as ConfigContextDto;

    return result;
  }

  async update(key: string, updateConfigDto: UpdateConfigDto) {
    const result = await this.prisma.config.update({ where: { key }, data: updateConfigDto });
    return result.id;
  }
}
