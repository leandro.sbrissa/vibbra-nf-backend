import { Controller, Get, Post, Body, Patch, Param, Query } from '@nestjs/common';
import { ApiQuery } from '@nestjs/swagger';

import { EmailService } from '../email/email.service';
import { SmsService } from '../sms/sms.service';
import { ConfigsService } from './configs.service';
import { RequestTesteConfigDto } from './dto/request-config.dto';
import { UpdateConfigDto } from './dto/update-config.dto';

@Controller('configs')
export class ConfigsController {
  constructor(
    private readonly configsService: ConfigsService,
    private readonly emailService: EmailService,
    private readonly smsService: SmsService,
  ) {}

  @ApiQuery({ type: RequestTesteConfigDto })
  @Get('/test')
  async testSend(@Query() query: RequestTesteConfigDto) {
    const { email, sms } = query;

    const emailSent =
      email &&
      (await this.emailService.sendEmail({
        to: email,
        subject: 'Teste de envio',
        message: 'Teste de envio de e-mail realizado',
      }));

    const smsSent =
      sms &&
      (await this.smsService.sendSms({
        to: sms,
        body: 'Teste de envio realizado',
      }));

    return {
      success: !!(emailSent && smsSent),
      query,
    };
  }

  @Get()
  async findAll() {
    const list = await this.configsService.findAll();
    const configs = list.reduce((acc, config) => {
      acc[config.key] = config.value;
      return acc;
    }, {});

    return {
      success: true,
      ...configs,
    };
  }

  @Post(':key')
  async store(@Param('key') key: string, @Body() body: any) {
    const value = body[key];
    const config = await this.configsService.store(key, value);
    return {
      success: true,
      config,
    };
  }

  @Get(':key')
  async findOne(@Param('key') key: string) {
    const config = await this.configsService.findOne(key);
    return {
      success: !!(config && config.value),
      [key]: (config && config.value) || {},
    };
  }

  @Patch(':key')
  update(@Param('key') key: string, @Body() updateConfigDto: UpdateConfigDto) {
    return this.configsService.update(key, updateConfigDto);
  }
}
