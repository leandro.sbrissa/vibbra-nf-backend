import { formatPrice } from '../../helpers/number';

export interface INotificationContext {
  toEmail?: string;
  toSms?: string;
  quota: number;
  maxRevenue: number;
  totalRevenue: number;
  quotaLimit: number;
  remainingTotal: number;
  remainingQuota: number;
}

export function monthlyText({ remainingTotal, maxRevenue }: INotificationContext): string {
  let message = '';
  if (remainingTotal > maxRevenue) {
    message += `Limite anual: ${formatPrice(maxRevenue)}. 
    Ainda resta ${formatPrice(remainingTotal)} em notas para o limite de ${formatPrice(maxRevenue)} configurado.`;
  } else {
    message += `Limite de faturamento de R$ ${formatPrice(maxRevenue)} configurado 
    ultrapassado em ${formatPrice(remainingTotal)}.`;
  }
  return message;
}

export function checkRevenueText({ quota, totalRevenue, maxRevenue }: INotificationContext): string {
  const message = `Seu faturamento anual já chegou a ${quota}% de ${formatPrice(maxRevenue)}.  
  Valor alcançado: ${formatPrice(totalRevenue)}`;
  return message;
}

export function remainingRevenueText({ remainingTotal }: INotificationContext): string {
  const message = `Faturamento restante para esse ano ${formatPrice(remainingTotal)}.`;
  return message;
}
