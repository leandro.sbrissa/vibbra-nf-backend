import { Module } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';

import { EmailModule } from '../email/email.module';
import { HistoriesModule } from '../histories/histories.module';
import { SmsModule } from '../sms/sms.module';
import { TasksService } from './tasks.service';

@Module({
  imports: [ScheduleModule.forRoot(), HistoriesModule, SmsModule, EmailModule],
  providers: [TasksService],
  exports: [TasksService],
})
export class NotificationModule {}
