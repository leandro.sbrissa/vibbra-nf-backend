import { Injectable, Logger } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';

import { EmailService } from '../email/email.service';
import { HistoriesService } from '../histories/histories.service';
import { SmsService } from '../sms/sms.service';
import { monthlyText, remainingRevenueText } from './tasks.helpers';

@Injectable()
export class TasksService {
  private readonly logger = new Logger(TasksService.name);

  constructor(
    private readonly historiesServices: HistoriesService,
    private readonly emailService: EmailService,
    private readonly smsService: SmsService,
  ) {}

  // @Cron(CronExpression.EVERY_10_SECONDS, { name: 'notifications' })
  @Cron(CronExpression.EVERY_1ST_DAY_OF_MONTH_AT_NOON, { name: 'monthly-task' })
  async monthlyNotifications() {
    const contextData = await this.historiesServices.getNotificationContext();
    if (contextData && contextData.toEmail) {
      const mailText = monthlyText(contextData);
      await this.emailService.sendEmail({
        to: contextData.toEmail,
        subject: 'Relatório mensal ✔',
        message: mailText,
      });
    }

    if (contextData && contextData.toSms) {
      const smsText = remainingRevenueText(contextData);
      await this.smsService.sendSms({
        to: contextData.toEmail,
        body: smsText,
      });
    }
  }
}
