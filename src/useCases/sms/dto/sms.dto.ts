export class SmsMessageDto {
  to: string;
  body: string;
}
