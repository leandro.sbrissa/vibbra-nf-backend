import { Injectable, OnModuleInit, Logger } from '@nestjs/common';
import { config } from 'dotenv';
import { Twilio } from 'twilio';

import { SmsMessageDto } from './dto/sms.dto';

config();

@Injectable()
export class SmsService implements OnModuleInit {
  private readonly logger = new Logger(SmsService.name);
  private client: Twilio;
  private from: string;

  constructor() {
    const accountSid = process.env.TWILIO_ACCOUNT_SID;
    const authToken = process.env.TWILIO_AUTH_TOKEN;
    this.from = process.env.TWILIO_PHONE_NUMBER;
    this.client = new Twilio(accountSid, authToken);
  }

  async onModuleInit() {
    this.logger.log(`init FROM NUMBER: ${this.from}`);
  }

  async sendSms(payload: SmsMessageDto): Promise<boolean> {
    const to = `${payload.to}`.replace(/[^\d]+/g, '');
    this.logger.log(`SENDING TO: +${to} ${payload.body}`);
    try {
      await this.client.messages.create({
        to: `+${to}`,
        body: payload.body,
        from: this.from,
      });
      return true;
    } catch (error) {
      this.logger.error(error);
      return false;
    }
  }
}
