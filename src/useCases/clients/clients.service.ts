import { BadRequestException, Injectable } from '@nestjs/common';

import { Prisma } from '@prisma/client';

import { PaginationQueryDto } from '../../prisma/paginate-prisma.dto';
import { PrismaService } from '../../prisma/prisma.service';
import { ClientDto, CreateClientDto } from './dto/create-client.dto';
import { UpdateClientDto } from './dto/update-client.dto';

export type ClientServiceFilterType = Prisma.ClientFindManyArgs['where'];

@Injectable()
export class ClientsService {
  constructor(private prisma: PrismaService) {}

  async create(createClientDto: CreateClientDto) {
    try {
      const client = await this.prisma.client.create({ data: createClientDto });
      return client.id;
    } catch (error) {
      throw new BadRequestException('invalid client data');
    }
  }

  async paginate(pagination: PaginationQueryDto, filter: ClientServiceFilterType) {
    const paginated = await this.prisma.paginate<ClientDto>({
      model: 'Client',
      ...pagination,
      where: { ...filter },
    });
    return paginated;
  }

  async findAll(query?: Prisma.ClientFindManyArgs) {
    const result = await this.prisma.client.findMany({ orderBy: { name: 'asc' }, ...query });
    return result as ClientDto[];
  }

  async findOne(id: number) {
    const client = await this.prisma.client.findUnique({ where: { id } });
    return client;
  }

  async update(id: number, updateClientDto: UpdateClientDto) {
    const client = await this.prisma.client.update({ where: { id }, data: updateClientDto });
    return client;
  }

  remove(id: number) {
    try {
      this.prisma.client.delete({ where: { id } });
      return true;
    } catch (error) {
      return false;
    }
  }
}
