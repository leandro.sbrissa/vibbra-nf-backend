import { Controller, Get, Post, Body, Patch, Param, Delete, ParseIntPipe } from '@nestjs/common';
import { ApiBearerAuth, ApiQuery, ApiTags } from '@nestjs/swagger';

import { Paginate } from '../../prisma/paginate-prisma.decorator';
import { ClientServiceFilterType, ClientsService } from './clients.service';
import { CreateClientDto } from './dto/create-client.dto';
import { RequestPaginateClientDto, ResponseClientDto, ResponsePaginateClientDto } from './dto/paginate-client.dto';
import { UpdateClientDto } from './dto/update-client.dto';

@ApiBearerAuth('JWT')
@ApiTags('clients')
@Controller('clients')
export class ClientsController {
  constructor(private readonly clientsService: ClientsService) {}

  @Post()
  async create(@Body() createClientDto: CreateClientDto) {
    const clientId = await this.clientsService.create(createClientDto);
    return {
      success: true,
      clientId,
    };
  }

  @ApiQuery({ type: RequestPaginateClientDto })
  @Get()
  async findAll(@Paginate() pagination: RequestPaginateClientDto): Promise<ResponsePaginateClientDto> {
    const { search, ...restPagination } = pagination;

    const where: ClientServiceFilterType = { id: { not: 0 } };
    if (search) {
      where.AND = {
        OR: [{ name: { contains: `%${search}%` } }, { socialName: { contains: `%${search}%` } }],
      };
    }
    const paginated = await this.clientsService.paginate(restPagination, where);
    return { success: true, ...paginated };
  }

  @Get(':id')
  async findOne(@Param('id', ParseIntPipe) id: number): Promise<ResponseClientDto> {
    const client = await this.clientsService.findOne(id);
    return {
      success: true,
      client,
    };
  }

  @Patch(':id')
  async update(@Param('id', ParseIntPipe) id: number, @Body() updateClientDto: UpdateClientDto) {
    const client = await this.clientsService.update(id, updateClientDto);
    return {
      success: true,
      clientId: client.id,
      client,
    };
  }

  @Delete(':id')
  async remove(@Param('id', ParseIntPipe) id: number) {
    const deleted = this.clientsService.remove(id);
    return {
      success: !!deleted,
    };
  }
}
