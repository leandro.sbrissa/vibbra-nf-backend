import { ApiProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';

import { PaginationQueryDto } from '../../../prisma/paginate-prisma.dto';
import { ResponseApiDto, ResponsePaginatedApi } from '../../app/dto/response-api.dto';
import { ClientDto } from './create-client.dto';

export class RequestPaginateClientDto extends PaginationQueryDto {
  @IsOptional()
  @ApiProperty({ description: 'filtro por texto', nullable: true })
  search?: string;
}

export class ResponsePaginateClientDto extends ResponsePaginatedApi {
  data?: ClientDto[];
}

export class ResponseClientDto extends ResponseApiDto {
  client?: ClientDto;
}
