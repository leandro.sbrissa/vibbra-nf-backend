import { ApiProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';

export class ClientDto {
  id: number;
  cnpj: string;
  name: string;
  socialName: string;
  actived?: boolean;
}

export class CreateClientDto {
  @ApiProperty({ description: 'CNJP do cliente' })
  cnpj: string;

  @ApiProperty({ description: 'Nome fantazia' })
  name: string;

  @ApiProperty({ description: 'Razão social' })
  socialName: string;

  @IsOptional()
  actived?: boolean;
}
