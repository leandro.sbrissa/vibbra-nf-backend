import { BadRequestException, Injectable } from '@nestjs/common';
import { getYear } from 'date-fns';

import { Prisma } from '.prisma/client';

import { round } from '../../helpers/number';
import { PaginationQueryDto } from '../../prisma/paginate-prisma.dto';
import { PrismaService } from '../../prisma/prisma.service';
import { ConfigsService } from '../configs/configs.service';
import { EmailService } from '../email/email.service';
import { SmsService } from '../sms/sms.service';
import { INotificationContext } from '../tasks/tasks.helpers';
import { CreateHistoryDto, HistoryDto } from './dto/create-history.dto';
import { HistorySummaryDto } from './dto/paginate-history.dto';
import { UpdateHistoryDto } from './dto/update-history.dto';

export type HistoryServiceFilterType = Prisma.ExpenseFindManyArgs['where'];

@Injectable()
export class HistoriesService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly configsService: ConfigsService,
    private readonly emailService: EmailService,
    private readonly smsService: SmsService,
  ) {}

  async getNotificationContext(): Promise<INotificationContext> {
    const configs = await this.configsService.findByKey(['revenue', 'notify']);
    if (!configs?.revenue || !configs?.notify) return null;
    const { maximum = 81000, quota } = configs.revenue;
    const { email, sms } = configs.notify;

    const totalRevenue = await this.sumByYear(getYear(new Date()));

    const contextData: INotificationContext = {
      toEmail: email?.enabled && email?.to,
      toSms: sms?.enabled && sms?.to,
      quota,
      maxRevenue: maximum,
      totalRevenue,
      quotaLimit: maximum && quota ? round((quota / 100) * maximum, 2) : 0,
      remainingTotal: round(maximum - totalRevenue, 2),
      remainingQuota: 0,
    };
    contextData.remainingQuota = round(contextData.quotaLimit - totalRevenue, 2);
    return contextData;
  }

  async checkLimits() {
    const contextData = await this.getNotificationContext();
    if (contextData.remainingQuota <= 0) {
      // send messages
      const message = `Conta de ${contextData.quota}% do faturamanto anual ultrapassada`;
      if (contextData.toEmail)
        await this.emailService.sendEmail({ to: contextData.toEmail, subject: 'Cota alcançada', message });
      if (contextData.toSms) await this.smsService.sendSms({ to: contextData.toSms, body: message });
    }
  }

  async paginate(pagination: PaginationQueryDto, filter: HistoryServiceFilterType) {
    const paginated = await this.prisma.paginate<HistoryDto>({
      model: 'History',
      ...pagination,
      where: { ...filter },
      include: {
        client: {
          select: { name: true, cnpj: true },
        },
      },
    });
    return paginated;
  }

  async create(createHistoryDto: CreateHistoryDto) {
    try {
      const history = await this.prisma.history.create({ data: createHistoryDto });
      if (history) this.checkLimits();
      return history.id;
    } catch (error) {
      throw new BadRequestException('invalid history data');
    }
  }

  async findOne(id: number) {
    const history = await this.prisma.history.findUnique({ where: { id } });
    return history;
  }

  async update(id: number, updateHistoryDto: UpdateHistoryDto) {
    const history = await this.prisma.history.update({ where: { id }, data: updateHistoryDto });
    if (history) this.checkLimits();
    return history;
  }

  async remove(id: number) {
    try {
      await this.prisma.history.delete({ where: { id } });
      return true;
    } catch (error) {
      return false;
    }
  }

  async sumByYear(year = 2021): Promise<number> {
    const histories = (await this.prisma.$queryRaw`
    SELECT SUM(value) AS total 
    FROM histories 
    WHERE YEAR(date) = ${year}`) as Array<{ total: number }>;
    return histories ? histories.reduce((acc, h) => (acc += h.total), 0) || 0 : 0;
  }

  async summary(year: number) {
    const summary = await this.prisma.$queryRaw`
    SELECT DISTINCT(month) AS month, 
    COUNT(id) AS COUNT,
    SUM(value) AS total
    FROM histories 
    WHERE YEAR(date) = ${year}
    GROUP BY month
    ORDER BY month ASC
    `;
    return summary as HistorySummaryDto[];
  }

  async years() {
    const summary = (await this.prisma.$queryRaw`
    SELECT DISTINCT(YEAR(date)) AS year 
    FROM histories 
    GROUP BY YEAR(date)
    ORDER BY YEAR(date) ASC;
    `) as Array<{ year: number }>;
    return summary.map(({ year }) => year) as number[];
  }
}
