import { Controller, Get, Post, Body, Patch, Param, Delete, ParseIntPipe } from '@nestjs/common';
import { ApiBearerAuth, ApiQuery, ApiTags } from '@nestjs/swagger';

import { Paginate } from '../../prisma/paginate-prisma.decorator';
import { CreateHistoryDto } from './dto/create-history.dto';
import {
  RequestPaginateHistoryDto,
  ResponseHistorySummaryDto,
  ResponseHistoryYearsDto,
} from './dto/paginate-history.dto';
import { UpdateHistoryDto } from './dto/update-history.dto';
import { HistoriesService, HistoryServiceFilterType } from './histories.service';

@ApiTags('histories')
@ApiBearerAuth('JWT')
@Controller('histories')
export class HistoriesController {
  constructor(private readonly historiesService: HistoriesService) {}

  @Get('/years')
  async getYears(): Promise<ResponseHistoryYearsDto> {
    const years = await this.historiesService.years();
    return {
      success: true,
      years,
    };
  }

  @Get('/sum/:year')
  async sumRevenue(@Param('year', ParseIntPipe) year: number) {
    const sum = await this.historiesService.sumByYear(year);
    return {
      success: true,
      sum,
    };
  }

  @Post()
  async create(@Body() createHistoryDto: CreateHistoryDto) {
    const historyId = await this.historiesService.create(createHistoryDto);
    return {
      success: true,
      historyId,
    };
  }

  @ApiQuery({ type: RequestPaginateHistoryDto })
  @Get()
  async findAll(@Paginate() pagination: RequestPaginateHistoryDto) {
    const { search, clientId, ...restPagination } = pagination;
    const where: HistoryServiceFilterType = {};
    if (clientId) {
      where.clientId = clientId;
    }
    if (search) {
      where.AND = {
        OR: [{ name: { contains: `%${search}%` } }],
      };
    }
    const paginated = await this.historiesService.paginate({ ...restPagination }, where);
    return { success: true, ...paginated };
  }

  @Get(':id')
  async findOne(@Param('id', ParseIntPipe) id: number) {
    const history = await this.historiesService.findOne(id);
    return {
      success: true,
      history,
    };
  }

  @Patch(':id')
  async update(@Param('id', ParseIntPipe) id: number, @Body() updateHistoryDto: UpdateHistoryDto) {
    const history = await this.historiesService.update(id, updateHistoryDto);
    return {
      success: true,
      historyId: history.id,
      history,
    };
  }

  @Delete(':id')
  async remove(@Param('id', ParseIntPipe) id: number) {
    this.historiesService.remove(id);
    return {
      success: true,
    };
  }

  @Get('/summary/:year')
  async summary(@Param('year', ParseIntPipe) year: number): Promise<ResponseHistorySummaryDto> {
    const summary = await this.historiesService.summary(year);

    return {
      success: true,
      summary,
    };
  }
}
