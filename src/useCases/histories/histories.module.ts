import { Module } from '@nestjs/common';

import { PrismaService } from '../../prisma/prisma.service';
import { ConfigsService } from '../configs/configs.service';
import { EmailModule } from '../email/email.module';
import { SmsModule } from '../sms/sms.module';
import { HistoriesController } from './histories.controller';
import { HistoriesService } from './histories.service';

@Module({
  imports: [SmsModule, EmailModule],
  controllers: [HistoriesController],
  providers: [PrismaService, HistoriesService, ConfigsService],
  exports: [HistoriesService],
})
export class HistoriesModule {}
