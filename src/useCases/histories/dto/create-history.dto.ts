import { IsDateString, IsInt, IsNumber, IsOptional, IsString } from 'class-validator';

export class HistoryDto {
  id: number;
  clientId: number;
  nfNumber: string;
  value: number;
  description: string;
  month: number;
  date: Date;
  readonly createdAt: Date;
  readonly updatedAt: Date;
}

export class CreateHistoryDto {
  @IsInt()
  clientId: number;

  @IsString()
  nfNumber: string;

  @IsNumber()
  value: number;

  @IsString()
  @IsOptional()
  description: string;

  @IsInt()
  month: number;

  @IsDateString()
  date: Date;
}
