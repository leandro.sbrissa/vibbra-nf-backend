import { ApiProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';

import { PaginationQueryDto } from '../../../prisma/paginate-prisma.dto';
import { ResponseApiDto } from '../../app/dto/response-api.dto';

export class HistorySummaryDto {
  month: Date;
  count: number;
  total: number;
}

export class ResponseHistorySummaryDto extends ResponseApiDto {
  summary: HistorySummaryDto[];
}

export class ResponseHistoryYearsDto extends ResponseApiDto {
  years: number[];
}
export class RequestPaginateHistoryDto extends PaginationQueryDto {
  @IsOptional()
  @ApiProperty({ description: 'filtro por texto', nullable: true })
  search?: string;
  clientId?: number;
}
