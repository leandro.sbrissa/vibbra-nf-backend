import { Module } from '@nestjs/common';
import { APP_GUARD } from '@nestjs/core';

import { PrismaModule } from '../../prisma/prisma.module';
import { AuthModule } from '../auth/auth.module';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';
import { CategoriesModule } from '../categories/categories.module';
import { ClientsModule } from '../clients/clients.module';
import { ConfigsModule } from '../configs/configs.module';
import { EmailModule } from '../email/email.module';
import { ExpensesModule } from '../expenses/expenses.module';
import { HistoriesModule } from '../histories/histories.module';
import { SmsModule } from '../sms/sms.module';
import { NotificationModule } from '../tasks/tasks.module';
import { TasksService } from '../tasks/tasks.service';
import { UsersModule } from '../users/users.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';
@Module({
  imports: [
    PrismaModule,
    EmailModule,
    SmsModule,
    AuthModule,
    UsersModule,
    ClientsModule,
    HistoriesModule,
    CategoriesModule,
    ExpensesModule,
    ConfigsModule,
    NotificationModule,
  ],
  controllers: [AppController],
  providers: [TasksService, AppService, { provide: APP_GUARD, useClass: JwtAuthGuard }],
})
export class AppModule {}
