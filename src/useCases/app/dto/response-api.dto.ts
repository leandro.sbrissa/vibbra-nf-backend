import { PaginationDetailDto } from '../../../prisma/paginate-prisma.dto';

export class ResponseApiDto {
  success?: boolean;
  statusCode?: number;
  message?: string;
}

export class ResponsePaginatedApi extends PaginationDetailDto {
  success?: boolean;
  statusCode?: number;
  message?: string;
}
