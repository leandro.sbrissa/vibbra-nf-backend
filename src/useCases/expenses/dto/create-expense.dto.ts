import { IsDateString, IsInt, IsNumber, IsOptional, IsString, MaxLength } from 'class-validator';

export class ExpenseDto {
  id: number;
  categoryId: number;
  clientId?: number;
  name: string;
  date: Date;
  payDate?: Date;
  value: number;
}

export class CreateExpenseDto {
  // id?: number;

  @IsInt()
  categoryId: number;

  @IsInt()
  @IsOptional()
  clientId?: number;

  @IsString()
  @MaxLength(100)
  name: string;

  @IsDateString()
  date: Date;

  @IsDateString()
  @IsOptional()
  payDate?: Date;

  @IsNumber()
  value: number;
}
