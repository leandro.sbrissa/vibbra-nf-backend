import { ApiProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';

import { PaginationQueryDto } from '../../../prisma/paginate-prisma.dto';
import { ResponseApiDto } from '../../app/dto/response-api.dto';

export class RequestPaginateExpenseDto extends PaginationQueryDto {
  @IsOptional()
  @ApiProperty({ description: 'filtro por texto', nullable: true })
  search?: string;
  categoryId?: number;
}

class ExpenseSummary {
  @ApiProperty({ description: 'numero de ocorrências' })
  count: number;
  @ApiProperty({ description: 'valor somado das ocorrências' })
  total: number;
}
export class ExpenseSummaryCategoryDto extends ExpenseSummary {
  categoryId: number;
  categoryLabel?: string;
}

export class ExpenseSummaryClientDto extends ExpenseSummary {
  clientId: number;
  clientLabel: string;
}

export class ExpenseSummaryDto extends ExpenseSummary {
  @ApiProperty()
  month: Date;
}

export class ResponseExpenseSummaryDto extends ResponseApiDto {
  @ApiProperty()
  summary: ExpenseSummaryDto[];
}

export class ResponseExpenseSummaryCategoryDto extends ResponseApiDto {
  @ApiProperty()
  summary: ExpenseSummaryCategoryDto[];
}

export class ResponseExpenseSummaryClientDto extends ResponseApiDto {
  @ApiProperty()
  summary: ExpenseSummaryClientDto[];
}
