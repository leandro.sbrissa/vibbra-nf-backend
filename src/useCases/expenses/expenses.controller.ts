import { Controller, Get, Post, Body, Patch, Param, Delete, ParseIntPipe } from '@nestjs/common';
import { ApiBearerAuth, ApiQuery, ApiTags } from '@nestjs/swagger';

import { Paginate } from '../../prisma/paginate-prisma.decorator';
import { CategoriesService } from '../categories/categories.service';
import { ClientsService } from '../clients/clients.service';
import { CreateExpenseDto } from './dto/create-expense.dto';
import {
  RequestPaginateExpenseDto,
  ResponseExpenseSummaryCategoryDto,
  ResponseExpenseSummaryClientDto,
  ResponseExpenseSummaryDto,
} from './dto/paginate-expense.dto';
import { UpdateExpenseDto } from './dto/update-expense.dto';
import { ExpenseServiceFilterType, ExpensesService } from './expenses.service';

@ApiBearerAuth('JWT')
@ApiTags('expenses')
@Controller('expenses')
export class ExpensesController {
  constructor(
    private readonly expensesService: ExpensesService,
    private readonly categoriesService: CategoriesService,
    private readonly clientsService: ClientsService,
  ) {}

  @Get('/summary/:year')
  async summary(@Param('year', ParseIntPipe) year: number): Promise<ResponseExpenseSummaryDto> {
    const summary = await this.expensesService.summary(year);

    return {
      success: true,
      summary,
    };
  }

  @Get('/summarycategory/:year')
  async summaryCategory(@Param('year', ParseIntPipe) year: number): Promise<ResponseExpenseSummaryCategoryDto> {
    const summaryExpenses = await this.expensesService.summaryCategory(year);
    const categories = await this.categoriesService.findAll({ select: { id: true, name: true } });

    const summary = summaryExpenses.map(expenses => {
      return {
        ...expenses,
        categoryLabel: categories.find(f => f.id === expenses.categoryId)?.name || '--',
      };
    });
    return {
      success: true,
      summary,
    };
  }

  @Get('/summaryclient/:year')
  async summaryClient(@Param('year', ParseIntPipe) year: number): Promise<ResponseExpenseSummaryClientDto> {
    const summaryExpenses = await this.expensesService.summaryClient(year);
    const clientIds = summaryExpenses.map(s => s.clientId);
    const clients = await this.clientsService.findAll({
      select: { id: true, name: true },
      where: { id: { in: clientIds } },
    });

    const summary = summaryExpenses.map(expenses => {
      return {
        ...expenses,
        clientLabel: clients.find(f => f.id === expenses.clientId)?.name || '--',
      };
    });
    return {
      success: true,
      summary,
    };
  }

  @Post()
  async create(@Body() createExpenseDto: CreateExpenseDto) {
    const expenseId = await this.expensesService.create(createExpenseDto);
    return {
      success: true,
      expenseId,
    };
  }

  @ApiQuery({ type: RequestPaginateExpenseDto })
  @Get()
  async findAll(@Paginate() pagination: RequestPaginateExpenseDto) {
    const { search, categoryId, ...restPagination } = pagination;
    const where: ExpenseServiceFilterType = { id: { not: 0 } };
    if (categoryId) {
      where.categoryId = categoryId;
    }
    if (search) {
      where.AND = {
        OR: [{ name: { contains: `%${search}%` } }],
      };
    }
    const paginated = await this.expensesService.paginate(restPagination, where);
    return { success: true, ...paginated };
  }

  @Get(':id')
  async findOne(@Param('id', ParseIntPipe) id: number) {
    const expense = await this.expensesService.findOne(id);
    return {
      success: true,
      expense,
    };
  }

  @Patch(':id')
  async update(@Param('id', ParseIntPipe) id: number, @Body() updateExpenseDto: UpdateExpenseDto) {
    const expense = await this.expensesService.update(id, updateExpenseDto);
    return {
      success: true,
      expenseId: expense.id,
      expense,
    };
  }

  @Delete(':id')
  async remove(@Param('id', ParseIntPipe) id: number) {
    await this.expensesService.remove(id);
    return {
      success: true,
    };
  }
}
