import { Injectable } from '@nestjs/common';

import { Prisma } from '.prisma/client';

import { PaginationQueryDto } from '../../prisma/paginate-prisma.dto';
import { PrismaService } from '../../prisma/prisma.service';
import { CreateExpenseDto, ExpenseDto } from './dto/create-expense.dto';
import { ExpenseSummaryCategoryDto, ExpenseSummaryClientDto, ExpenseSummaryDto } from './dto/paginate-expense.dto';
import { UpdateExpenseDto } from './dto/update-expense.dto';

export type ExpenseServiceFilterType = Prisma.ExpenseFindManyArgs['where'];

@Injectable()
export class ExpensesService {
  constructor(private prisma: PrismaService) {}

  async paginate(pagination: PaginationQueryDto, filter: ExpenseServiceFilterType) {
    const paginated = await this.prisma.paginate<ExpenseDto>({
      model: 'Expense',
      ...pagination,
      where: { ...filter },
    });
    return paginated;
  }

  async create(createExpenseDto: CreateExpenseDto) {
    const result = await this.prisma.expense.create({ data: createExpenseDto });
    return result.id;
  }

  async findAll() {
    const result = await this.prisma.expense.findMany();
    return result;
  }

  async findOne(id: number) {
    const result = await this.prisma.expense.findUnique({ where: { id } });
    return result;
  }

  async update(id: number, updateCategoryDto: UpdateExpenseDto) {
    const result = await this.prisma.expense.update({ where: { id }, data: updateCategoryDto });
    return result;
  }

  async remove(id: number) {
    try {
      await this.prisma.expense.delete({ where: { id } });
      return true;
    } catch (error) {
      return false;
    }
  }

  async summary(year: number) {
    const summary = await this.prisma.$queryRaw`
    SELECT DISTINCT(MONTH(date)) AS month, 
    COUNT(id) AS COUNT,
    SUM(value) AS total
    FROM expenses 
    WHERE YEAR(date) = ${year}
    GROUP BY MONTH(date)
    ORDER BY MONTH(date) ASC
    `;
    return summary as ExpenseSummaryDto[];
  }

  async summaryCategory(year: number) {
    const summary = await this.prisma.$queryRaw`
    SELECT DISTINCT(categoryId) AS categoryId, 
    COUNT(id) AS COUNT,
    SUM(value) AS total
    FROM expenses 
    WHERE YEAR(date) = ${year}
    GROUP BY categoryId 
    ORDER BY SUM(value) DESC
    `;
    return summary as ExpenseSummaryCategoryDto[];
  }

  async summaryClient(year: number) {
    const summary = await this.prisma.$queryRaw`
    SELECT DISTINCT(clientId) AS clientId, 
    COUNT(id) AS COUNT,
    SUM(value) AS total
    FROM expenses 
    WHERE YEAR(date) = ${year} AND clientId > 0
    GROUP BY clientId 
    ORDER BY SUM(value) DESC
    `;
    return summary as ExpenseSummaryClientDto[];
  }
}
