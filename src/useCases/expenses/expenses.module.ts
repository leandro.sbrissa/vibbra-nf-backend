import { Module } from '@nestjs/common';

import { PrismaService } from '../../prisma/prisma.service';
import { CategoriesService } from '../categories/categories.service';
import { ClientsService } from '../clients/clients.service';
import { ExpensesController } from './expenses.controller';
import { ExpensesService } from './expenses.service';

@Module({
  controllers: [ExpensesController],
  providers: [PrismaService, ExpensesService, CategoriesService, ClientsService],
})
export class ExpensesModule {}
