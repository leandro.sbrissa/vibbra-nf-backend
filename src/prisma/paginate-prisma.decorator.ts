import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { Request } from 'express';

import { PaginationQueryDto } from './paginate-prisma.dto';

export const Paginate = createParamDecorator((_data: unknown, ctx: ExecutionContext): PaginationQueryDto => {
  const request: Request = ctx.switchToHttp().getRequest();
  const { query, body } = request;

  const { page = 1, size = 10, ...filter } = query;

  const result = {
    page: parseInt(page || body.page, 10) || undefined,
    size: parseInt(size || body.size, 10) || undefined,
    ...filter,
    // order: order && order.length ? order : undefined,
  };

  return result;
});
