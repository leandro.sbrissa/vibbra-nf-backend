import { INestApplication, Injectable, OnModuleInit, OnModuleDestroy } from '@nestjs/common';
import { config } from 'dotenv';

import { Prisma, PrismaClient } from '@prisma/client';

import { PaginationDto, PaginationQueryDto } from './paginate-prisma.dto';

config();

interface Props extends PaginationQueryDto {
  model: Prisma.ModelName;
  size?: number;
  page?: number;
  orderBy?: string;
  order?: 'asc' | 'desc';
  include?: any;
  where?: any;
}

function getDbUrl(): string {
  return process.env.DATABASE_URL;
}

@Injectable()
export class PrismaService extends PrismaClient implements OnModuleInit, OnModuleDestroy {
  constructor() {
    super({
      // log: ['query'],
      datasources: {
        db: {
          url: getDbUrl(),
        },
      },
    });
  }

  async onModuleInit() {
    await this.$connect();
  }

  async onModuleDestroy() {
    await this.$disconnect();
  }

  async enableShutdownHooks(app: INestApplication) {
    this.$on('beforeExit', async () => {
      await app.close();
    });
  }

  async paginate<T = unknown>({
    model,
    size = 25,
    page = 1,
    orderBy,
    order,
    include,
    where,
  }: Props): Promise<PaginationDto<T>> {
    const prismaModel = this[model.toLowerCase()];
    const totalCount = await prismaModel.count({ where: { ...where } });

    if (!totalCount) return { total: 0, pages: 0, page, data: [] };

    let findManyArgs = {};
    if (where) findManyArgs = { ...findManyArgs, where: { ...where } };
    if (size) {
      const skip = page > 1 ? size * page : undefined;
      findManyArgs = { ...findManyArgs, take: size, skip };
    }
    if (orderBy) findManyArgs = { ...findManyArgs, orderBy: { [orderBy]: order } };

    if (include) findManyArgs = { ...findManyArgs, include: include };

    const pages = Math.ceil(totalCount / size);

    try {
      const results = await prismaModel.findMany({ ...findManyArgs, take: size });
      return {
        total: totalCount,
        page,
        pages,
        data: results,
      };
    } catch (error) {
      return {
        total: 0,
        page,
        pages,
        data: [],
      };
    }
  }

  buildSelectFileds<Model = unknown>(fields: (keyof Model)[]) {
    const result = {} as Record<keyof Model, boolean>;
    for (let index = 0; index < fields.length; index++) {
      const element = fields[index];
      result[element] = true;
    }
    return result;
  }
}
