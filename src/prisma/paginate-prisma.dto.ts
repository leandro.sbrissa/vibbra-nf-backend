import { ApiProperty } from '@nestjs/swagger';
import { IsOptional } from 'class-validator';

export const orderEnum = ['desc', 'asc'] as const;
export class PaginationQueryDto {
  @ApiProperty({ description: 'numero da página', nullable: true })
  @IsOptional()
  page?: number;

  @ApiProperty({ description: 'tamanho da página de dados', nullable: true })
  @IsOptional()
  size?: number;

  @IsOptional()
  @ApiProperty({ enum: ['asc', 'desc'], nullable: true })
  order?: typeof orderEnum[number];

  @IsOptional()
  orderBy?: string;
}

export class PaginationDetailDto {
  total: number;
  pages: number;
  page: number;
}

export class PaginationDto<T extends Record<string, any>> extends PaginationDetailDto {
  data: T[];
}
